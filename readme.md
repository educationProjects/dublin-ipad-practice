# Dublin Apps
 
 Site of Dublin IT company that create Applications for Ipads, Iphones, etc.

 Pages:
 - Home page
 - Product Demo
 - Blog Page
 - Contact page

 Site should be adaptive, for mobile devices, tablets, medium screen's, large screens, extra large screens (4K)

 For implementation could be used any plugin's, libraries, frameworks.

 ## Home Page
- Hero Slider
 ![Slider][1]
 Should contain 3 slides, other 2 slides content can be choosed by developer

- Text Slider with links
 ![Slider][2]
 Should contain 3 slides, other 2 slides content can be choosed by developer
 
- Accordion 
 ![Slider][3]
 Create 3 Accordions


 [1]: images/home1.png
 [2]: images/home2.png
 [3]: images/home3.png